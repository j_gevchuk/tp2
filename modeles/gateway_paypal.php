<?php

class Paypal{

    public $idCommande;

    public $httpRequest;
    public $httpResponse;

    public $METHOD;
    public $USER;
    public $PWD;
    public $SIGNATURE;
    public $VERSION;
    public $PAYMENTACTION;
    public $IPADDRESS;
    public $CREDITCARDTYPE;
    public $ACCT;
    public $EXPDATE;
    public $CVV2;
    public $FIRSTNAME;
    public $LASTNAME;
    public $STREET;
    public $CITY;
    public $STATE;
    public $COUNTRYCODE;
    public $CURRENCYCODE;
    public $ITEMAMT;
    public $SHIPPINGAMT;
    public $TAXAMT;
    public $AMT;
    public $ZIP;
    public $DESC;

    public $transaction_id;
    public $avs_code;
    public $cvn_match;

    public $success;
    public $output_msg; 

    // Set sandbox (test mode) to true/false.
    Public $sandbox = TRUE;

    public $API_Endpoint;
    public $api_version;
    public $api_username;
    public $api_password;
    public $api_signature;

    public $labels = array(
      'unexpected_error'                    => "Cette transaction ne peut être complétée.",
      'transaction_success'                 => "Transaction complétée.",
      'verif_code_invalid'                  => "Cette transaction ne peut être complétée. Le code de vérification est invalide.",
      'credit_card_expired'                 => "Cette transaction ne peut être complétée. Veuillez utiliser une carte de crédit valide.",
      'invalid_expiration_date'             => "Cette transaction ne peut être complétée. Veuillez entrer une date d'expiration valide.",
      'credit_card_number_invalid'          => "Cette transaction ne peut être complétée. Veuillez entrer un numéro de carte valide.",
      'max_amount_exceeded'                 => "Cette transaction ne peut être complétée. (maximum autorisé)."
      );


    public function __construct($idCommande){
        $this->setParametres();
        $this->idCommande = $idCommande;
        $this->METHOD = "DoDirectPayment";
        $this->VERSION = $this->api_version;
        $this->USER = $this->api_username;
        $this->PWD = $this->api_password;
        $this->SIGNATURE = $this->api_signature;
        $this->PAYMENTACTION = 'Sale'; 
        $this->output_msg = array();


    } // __construct

    public function setParametres(){
      // Set PayPal API version and credentials.
      $this->api_version = '85.0';
      $this->API_Endpoint = $this->sandbox ? 'https://api-3t.sandbox.paypal.com/nvp' : 'https://api-3t.paypal.com/nvp';
      $this->api_username = $this->sandbox ? 'olga.zlotea.web-facilitator_api1.gmail.com' : 'LIVE_USERNAME_GOES_HERE';
      $this->api_password = $this->sandbox ? '1373030187' : 'LIVE_PASSWORD_GOES_HERE';
      $this->api_signature = $this->sandbox ? 'AMKF.28HexEBJq3Fx3vfVxrPsKZ1ANgxQdavw2UkfQa.3M-D3cHijnS9' : 'LIVE_SIGNATURE_GOES_HERE';

    }
    public function prepare_checkout_request(){

    $this->httpRequest .=
        "METHOD=" . urlencode($this->METHOD) .
        "&USER=" . urlencode($this->USER) .
        "&PWD=" . urlencode($this->PWD) .
        "&SIGNATURE=" . urlencode($this->SIGNATURE) .
        "&VERSION=" . urlencode($this->VERSION) .
        "&PAYMENTACTION=" . urlencode($this->PAYMENTACTION) .
        "&IPADDRESS=" . urlencode($this->IPADDRESS) .
        "&CREDITCARDTYPE=" . urlencode($this->CREDITCARDTYPE) .
        "&ACCT=" . urlencode($this->ACCT) .
        "&EXPDATE=" . urlencode($this->EXPDATE) .
        "&CVV2=" . urlencode($this->CVV2) .
        "&FIRSTNAME=" . urlencode($this->FIRSTNAME) .
        "&LASTNAME=" . urlencode($this->LASTNAME) .
        "&STREET=" . urlencode($this->STREET) .
        "&CITY=" . urlencode($this->CITY) .
        "&STATE=" . urlencode($this->STATE) .
        "&COUNTRYCODE=" . urlencode($this->COUNTRYCODE) .
        "&CURRENCYCODE=" . urlencode($this->CURRENCYCODE) .
        "&ITEMAMT=" . urlencode($this->ITEMAMT) .
        "&SHIPPINGAMT=" . urlencode($this->SHIPPINGAMT) .
        "&TAXAMT=" . urlencode($this->TAXAMT) .
        "&AMT=" . urlencode($this->AMT) .
        "&ZIP=" . urlencode($this->ZIP) .
        "&DESC=" . urlencode($this->DESC);

    } //prepare_checkout_request

    public function process_request(){
    // Set the curl parameters.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->API_Endpoint);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);

    // Turn off the server and peer verification (TrustManager Concept).
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);

    // Set the request as a POST FIELD for curl.
    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->httpRequest);

    // Get response from the server.
    $this->httpResponse = curl_exec($ch);

    if(!$this->httpResponse) {
      $methodName = "";
      // la requete n'est pas passée retrourner l'erreur
      trigger_error("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')', E_USER_WARNING);
      $this->success = false;

      $this->httpResponse  = -1;
    }

    return $this->httpResponse;
  } // process_request

  public function interpret_response(){

    if ($this->httpResponse !== -1){
      // decode response
      $this->httpResponse = urldecode($this->httpResponse);

      // Extract the response details.
      $httpResponseAr = explode("&", $this->httpResponse);

      /* Construire un tableau avec les valeurs de retour séparées par &. */
      $httpParsedResponseAr = array();
      foreach ($httpResponseAr as $i => $value) {
        $tmpAr = explode("=", $value);
        if(sizeof($tmpAr) > 1) {
          $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
        }
      }

      /* Vérifier si le retour est en erreur ou en succès et afficher les messages correspondant. */
      if($httpParsedResponseAr['ACK'] === 'Success' || $httpParsedResponseAr['ACK'] === 'SuccessWithWarning'){
        $this->avs_code = $httpParsedResponseAr['AVSCODE'];
        $this->cvn_match = $httpParsedResponseAr['CVV2MATCH'];
        $this->transaction_id = $httpParsedResponseAr['TRANSACTIONID'];
        array_push($this->output_msg, 'transaction_success');
        $this->success =  true;
      }else{
        $error_code = $httpParsedResponseAr['L_ERRORCODE0'];

        switch ($error_code) :
          case '10762':          $this->output_msg[$error_code] =  $this->labels['verif_code_invalid'];  break;
          case '10502':          $this->output_msg[$error_code] =  $this->labels['credit_card_expired']; break;
          case '10508':          $this->output_msg[$error_code] =  $this->labels['invalid_expiration_date']; break;
          case '10527':          $this->output_msg[$error_code] =  $this->labels['credit_card_number_invalid']; break;
          case '10552':          $this->output_msg[$error_code] =  $this->labels['max_amount_exceeded']; break;
          case '10558':          $this->output_msg[$error_code] =  $this->labels['credit_card_currency_unsupported']; break;
          default:
            $msg = "";
            foreach($httpParsedResponseAr as $key => $value){
              $msg .= "$key : ". $value ."\n";
            }
            // trigger_error("Paypal returned an unexpected error :\n" . $msg, E_USER_WARNING);
            $this->output_msg[$error_code] =  $this->labels['unexpected_error'];

          break;
        endswitch;

          // trigger_error("Paypal error :\n" . print_r($this->output_msg, true), E_USER_WARNING);
          $this->success = false;
      }
      // Enregistrer dans la BD Avec l'id de la commande ($this->idCommande)
    }


  } //interpret_response


}


class Gateway{

    public $paypal; 
    // Attention : le paramètre idCommande ne doit pas avoir une valeur par défaut. 
    // Ce paramètre doit correspondre à un ID existant dans votre BD
    function process_paypal_transaction($posted, $idCommande){

        $this->paypal = new Paypal($idCommande);

        $this->paypal = $this->feed_paypal_request($posted);
        $this->paypal->prepare_checkout_request();

        // REGULAR FLOW
        $this->paypal->process_request();
        $this->paypal->interpret_response();
        // REGULAR FLOW

        
        // ===========================================================
        // = if paypal return errors, throw them                =
        // ===========================================================
        if( !$this->paypal->success ):
            $output = array(
             'overall_status'=> 'paypal_error',
             'results'=> $this->paypal->output_msg
            );
        else:
           // =============================================================
           // = otherwise : success
           // =============================================================
           $output = array(
           'overall_status'=> 'success',
           'results'=> $this->paypal->output_msg
            );
           // -> send email (client)
           // -> send email (marchand)
         endif;

        if($this->paypal->AMT == 0) {
            $this->paypal->success = true;
            $this->paypal->output_msg = array('Bravo!');
            $output = array(
               'overall_status'=> 'success',
               'results'=> ""
            );
        }

         // prepare response for screen
        $this->paypal = $this->paypal;

        return $output;

   } // process_paypal_transaction

   function feed_paypal_request($posted){

        $this->paypal->IPADDRESS= $posted['IPADDRESS'];
        $this->paypal->CREDITCARDTYPE= $posted['CREDITCARDTYPE'];
        $this->paypal->ACCT= $posted['ACCT'];
        $this->paypal->EXPDATE= $posted['EXPDATE'];
        $this->paypal->CVV2= $posted['CVV2'];
        $this->paypal->FIRSTNAME= $posted['FIRSTNAME'];
        $this->paypal->LASTNAME= $posted['LASTNAME'];
        $this->paypal->STREET= $posted['STREET'];
        $this->paypal->CITY= $posted['CITY'];
        $this->paypal->STATE= $posted['STATE'];
        $this->paypal->COUNTRYCODE= $posted['COUNTRYCODE'];
        $this->paypal->CURRENCYCODE= $posted['CURRENCYCODE'];
        $this->paypal->ITEMAMT= $posted['ITEMAMT'];
        $this->paypal->SHIPPINGAMT= $posted['SHIPPINGAMT'];
        $this->paypal->TAXAMT= $posted['TAXAMT'];
        $this->paypal->AMT= $posted['AMT'];
        $this->paypal->ZIP= $posted['ZIP'];
        $this->paypal->DESC= $posted['DESC'];

        return $this->paypal;
    } // feed_paypal_request

}

?>