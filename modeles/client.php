<?php 
class Client {
	private $login, $password, $nomClient, $prenomClient, $telephon,  $adresse;
		function __construct($login,$password,$nomClient, $prenomClient, $telephon, $adresse){
			$this->login = $login;
			$this->password = $password;
			$this->nomClient = $nomClient;
			$this->prenomClient = $prenomClient;
			$this->telephon = $telephon;
			$this->adresse = $adresse;
		}

		public function getLogin(){
			return $this->login;
		}
		public function setLogin($login){
			$this->login = $login;
		}
		public function getPassword(){
			return $this->password;
		}
		public function setPassword($password){
			$this->password = $password;
		}
		public function getNomClient(){
			return $this->nomClient;
		}
		public function setNomClient($nomClient){
			$this->nomClient = $nomClient;
		}
		public function getpreNomClient(){
			return $this->prenomClient;
		}
		public function setpreNomClient($prenomClient){
			$this->prenomClient = $prenomClient;
		}
		public function getTelephon(){
			return $this->telephon;
		}
		public function setTelephon($telephon){
			$this->telephon = $telephon;
		}	
		public function getAdresse(){
			return $this->adresse;
		}
		public function setAdresse($adresse){
			$this->adresse = $adresse;
		}
}
 ?>