<?php 

class ligneCommande {
	private $idCommande;
	private $idProduit;
	private $quantite;
	private $prixUnite;
	
	public function __construct($commande, $produit, $prix, $quantite){
		$this->idCommande = $produit;
		$this->idProduit = $produit;
		$this->prixUnite = $prix;
		$this->quantite = $quantite;
	}

	public function getPrixUnite(){
		return $this->prixUnite;
	}

	public function getQuantite(){
		return $this->quantite;
	}
}
 ?>