<?php 

class Modele_Commande {
	private $db;
	private $commande;
	private $client;
	private $dateCommande;
	private $montant;
	private static $TPS = 0.05;
	private static $TVQ = 0.098;
	private $prixTotal; 
	private $livraison;
	private $ligneCommandes = array();
	private static $statusPaypal = 0;
	private static $statusTraitement = 0;

	//Instancier le modèle
	public function __construct(){
		$this->db = new AccessBD();
		$this->dateCommande = date('Y-m-d');
		$this->client = $_SESSION['user'];
	}

	//Method ajoute un produit a la bd
	public function ajouterCommande(){
		$connexion = $this->db->connecter();
		$requete = "INSERT INTO commande (dateCommande, idClient, statusPaypal, statusTraitement) VALUES (:dateCommande, :client, :paypal, :traitement);";
		$aVars = array(':dateCommande' => $this->dateCommande,
						':client' => $this->client,
						':paypal' => self::$statusPaypal,
						':traitement' => self::$statusTraitement
			);
		$idCommande = AccessBD::preparerID($connexion, $requete, $aVars);
		return $idCommande;
	}

	public function ajouterLigneCommande($commande){
		foreach ($_SESSION['panier'] as $key => $value) {
			$produit = $key;
			$prixUnite = $value['prix'];
			$quantite = $value['quantite'];
			$connexion = $this->db->connecter();
			$requete = "INSERT INTO lignedecommande (idProd, idCommande, prixUnite, quantite) VALUES ('$key', '$commande', '$prixUnite', '$quantite');";
			if($this->db->preparer($connexion, $requete)){
				$ligne = new ligneCommande($commande, $produit, $prixUnite, $quantite);
				$this->ligneCommandes[] = $ligne;
			}
		}
		return $this->ligneCommandes;
	}


	public static function calculerMontant($lignes){
		$total = 0;
		foreach ($lignes as $obj) {
			$total += $obj->getPrixUnite() * $obj->getQuantite();
		}
		return $total;
	}

	public static function calculerLivraison($montant){
		if($montant < 50){
			$livraison = 10;
		}
		elseif ($montant >= 50 && $montant < 100){
			$livraison = 5;
		}
		else {
			$livraison = 0;
		}
		return $livraison;
	}

	public function calculerTPS($montant){
		$TPS = $montant * self::$TPS;
		return $TPS;
	}

	public function calculerTVQ($montant){
		$TVQ = $montant * self::$TVQ;
		return $TVQ;
	}

	public static function calculerTotal($montant, $livraison, $TPS, $TVQ){
		return $montant + $livraison + $TPS + $TVQ;
	}


	public function completerCommande($commande, $montant, $TPS, $TVQ, $livraison, $total){
		$connexion = $this->db->connecter();
		$requete = "UPDATE commande SET montant = :montant, montLivraison = :livraison, montTotal = :total, TPS = :TPS, TVQ = :TVQ WHERE idCommande = :commande;";
		$aVars = array(':montant' => $montant,
						':livraison' => $livraison,
						':total' => $total,
						':TPS' => $TPS,
						':TVQ' => $TVQ,
						':commande' => $commande);
		$this->db->preparer($connexion, $requete, $aVars);
	}

	public function misajourStatus($idCommande){
		echo $idCommande;
		$connexion = $this->db->connecter();
		$sReq = "UPDATE commande set statusPaypal = 1 WHERE idCommande = $idCommande;";
		$this->db->preparer($connexion, $sReq);
	}


}
 ?>