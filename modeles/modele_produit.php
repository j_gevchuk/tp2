<?php 
/**
 * Fichier: 'modele_produit.php'
 * * @author  Iulianna Gevchuk
 * Description: classe Produits qui récupère des données de la BD
 *
 */

class Produits {
	//Fonction récupère tous les produits de la base de données 
	public function getListeProduits(){
		$db = new AccessBD();
		$connexion = $db->connecter();
		//Récupérer des articles disponibles
		$requete = 'SELECT * FROM produits WHERE disponibilite = 1';
		$resultats = $db->select($connexion, $requete);
		return $resultats;
	}
}
 ?>
