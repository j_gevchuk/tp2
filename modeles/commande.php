<?php 

class Commande {
	private $db;
	private $commande;
	private $client;
	private $dateCommande;
	//Taxes
	private static $TPS = 0.05;
	private static $TVQ = 0.098;
	//Tableau des lignes de la commande
	private $lignes = array();
	private static $statusPaypal = 0;
	private static $statusTraitement = 0;

	//Instancier le modèle
	public function __construct(){
		$this->db = new AccessBD();
		//Créér un objet Date
        $this->dateCommande = date('Y-m-d');
        //Récupèrer l'id de client de la SESSION
        $this->client = $_SESSION['user'];
	}

	//Ajouter une nouvelle commande à la BD et retourner son id
	public function ajouterCommande(){
		$connexion = $this->db->connecter();
        $requete = "INSERT INTO commande (dateCommande, idClient, statusPaypal, statusTraitement) VALUES (:dateCommande, :client, :paypal, :traitement);";
        $aVars = array(':dateCommande' => $this->dateCommande,
                        ':client' => $this->client,
                        ':paypal' => self::$statusPaypal,
                        ':traitement' => self::$statusTraitement
                        );
        $idCommande = AccessBD::preparerID($connexion, $requete, $aVars);
        return $idCommande;
	}

	//Insérer toutes les lignes de la commande dans la BD à partir de la session 'panier'
	public function ajouterLigneCommande($commande){
        foreach ($_SESSION['panier'] as $key => $value) {
                $produit = $key;
                $prixUnite = $value['prix'];
                $quantite = $value['quantite'];
                $connexion = $this->db->connecter();
                $requete = "INSERT INTO lignedecommande (idProd, idCommande, prixUnite, quantite) VALUES ('$key', '$commande', '$prixUnite', '$quantite');";
                if($this->db->preparer($connexion, $requete)){
                		//Créér un tableau des objets
                        $ligne = new ligneCommande($commande, $produit, $prixUnite, $quantite);
                        $this->ligneCommandes[] = $ligne;
                }
        }
        return $this->ligneCommandes;
	}

	//Calculer le montant
    public static function calculerMontant($lignes){
        $total = 0;
        foreach ($lignes as $obj) {
                $total += $obj->getPrixUnite() * $obj->getQuantite();
        }
        return $total;
    }

	//Calculer la livraison
    public static function calculerLivraison($montant){
        if($montant < 50){
                $livraison = 10;
        }
        elseif ($montant >= 50 && $montant < 100){
                $livraison = 5;
        }
        else {
                $livraison = 0;
        }
        return $livraison;
    }

    //Calculer le TPS
    public function calculerTPS($montant){
        $TPS = $montant * self::$TPS;
        return $TPS;
    }

    //Calculer le TVQ
    public function calculerTVQ($montant){
        $TVQ = $montant * self::$TVQ;
        return $TVQ;
    }

    //Calculer le coût total
    public static function calculerTotal($montant, $livraison, $TPS, $TVQ){
        return $montant + $livraison + $TPS + $TVQ;
    }

    //Mettre à jour la commande dans la BD
    public function completerCommande($commande, $montant, $TPS, $TVQ, $livraison, $total){
        $connexion = $this->db->connecter();
        $requete = "UPDATE commande SET montant = :montant, montLivraison = :livraison, montTotal = :total, TPS = :TPS, TVQ = :TVQ WHERE idCommande = :commande;";
        $aVars = array(':montant' => $montant,
                        ':livraison' => $livraison,
                        ':total' => $total,
                        ':TPS' => $TPS,
                        ':TVQ' => $TVQ,
                        ':commande' => $commande);
        $this->db->preparer($connexion, $requete, $aVars);
    }

    //Mettre à jour le status PayPal 
    public function misajourStatus($idCommande){
        echo $idCommande;
        $connexion = $this->db->connecter();
        $sReq = "UPDATE commande set statusPaypal = 1 WHERE idCommande = $idCommande;";
        $this->db->preparer($connexion, $sReq);
    }
}
 ?>