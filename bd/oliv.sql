-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
<<<<<<< HEAD
-- Client: 127.0.0.1
-- Généré le: Jeu 11 Juillet 2013 à 21:17
-- Version du serveur: 5.5.27-log
-- Version de PHP: 5.4.6
=======
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2013 at 02:55 AM
-- Server version: 5.5.27-log
-- PHP Version: 5.4.6
>>>>>>> 9574641495e2dc0c267a35452f311e923cec08e6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `oliv`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `emailAdmin` varchar(50) NOT NULL,
  `passwordAdmin` varchar(255) DEFAULT NULL,
  `nameAdmin` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`emailAdmin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`emailAdmin`, `passwordAdmin`, `nameAdmin`) VALUES
('olga.zlotea.web@gmai', '123', 'Olga');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `idClient` int(255) NOT NULL AUTO_INCREMENT,
  `courriel` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `pays` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idClient`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `clients`
--

INSERT INTO `clients` (`idClient`, `courriel`, `password`, `nom`, `prenom`, `telephone`, `pays`) VALUES
(1, 'nato@intv.ru', 'fuckoff', 'Hui', 'Vanderhui', '(345)-444-4567', 'Canada'),
(3, 'dima@gmail.com', '123', 'Zlot', 'Olga', '4445555666', NULL),
(4, 'jhg@fflkh.cvg', '333', 'Drom', 'Bom', '3335555777', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE IF NOT EXISTS `commande` (
  `idCommande` int(11) NOT NULL AUTO_INCREMENT,
  `dateCommande` date DEFAULT NULL,
  `montant` float DEFAULT NULL,
  `montLivraison` float DEFAULT NULL,
  `montTotal` float DEFAULT NULL,
  `idClient` int(11) NOT NULL,
  `TPS` float NOT NULL,
  `TVQ` float NOT NULL,
  `statusPaypal` tinyint(4) NOT NULL,
  `statusTraitement` tinyint(4) NOT NULL,
  PRIMARY KEY (`idCommande`),
  KEY `FK_commande_idClient` (`idClient`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Contenu de la table `commande`
--

INSERT INTO `commande` (`idCommande`, `dateCommande`, `montant`, `montLivraison`, `montTotal`, `idClient`, `TPS`, `TVQ`, `statusPaypal`, `statusTraitement`) VALUES
(4, '2013-07-08', 32, NULL, 36.64, 1, 0, 0, 0, 0),
(124, '2013-07-09', 50, 5, 106.5, 1, 2.5, 49, 0, 0),
(125, '2013-07-09', 50, 5, 62.4, 1, 2.5, 4.9, 0, 0),
<<<<<<< HEAD
(126, '2013-07-11', NULL, NULL, NULL, 1, 0, 0, 0, 0),
(127, '2013-07-11', NULL, NULL, NULL, 1, 0, 0, 0, 0),
(128, '2013-07-11', 50, 5, 62.4, 1, 2.5, 4.9, 1, 1);
=======
(126, '2013-07-09', 192, 0, 220.416, 1, 9.6, 18.816, 0, 0),
(127, '2013-07-09', 192, 0, 220.416, 1, 9.6, 18.816, 0, 0),
(128, '2013-07-11', 178, 0, 204.344, 1, 8.9, 17.444, 0, 0);
>>>>>>> 9574641495e2dc0c267a35452f311e923cec08e6

-- --------------------------------------------------------

--
-- Structure de la table `lignedecommande`
--

CREATE TABLE IF NOT EXISTS `lignedecommande` (
  `idProd` int(11) NOT NULL AUTO_INCREMENT,
  `idCommande` int(11) NOT NULL,
  `quantite` int(3) DEFAULT NULL,
  `prixUnite` float NOT NULL,
  PRIMARY KEY (`idProd`,`idCommande`),
  KEY `FK_ligneDeCommande_id` (`idCommande`)
<<<<<<< HEAD
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;
=======
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;
>>>>>>> 9574641495e2dc0c267a35452f311e923cec08e6

--
-- Contenu de la table `lignedecommande`
--

INSERT INTO `lignedecommande` (`idProd`, `idCommande`, `quantite`, `prixUnite`) VALUES
(1, 125, 1, 32),
<<<<<<< HEAD
(1, 128, 1, 32),
(2, 125, 1, 18),
(2, 128, 1, 18),
(4, 126, 2, 32),
(4, 127, 1, 34),
(5, 127, 2, 32);
=======
(1, 126, 6, 32),
(1, 127, 6, 32),
(1, 128, 5, 32),
(2, 125, 1, 18),
(2, 128, 1, 18);
>>>>>>> 9574641495e2dc0c267a35452f311e923cec08e6

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE IF NOT EXISTS `produits` (
  `idProd` int(20) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `prix` varchar(255) DEFAULT NULL,
  `url_media` varchar(255) DEFAULT NULL,
  `disponibilite` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idProd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `produits`
--

INSERT INTO `produits` (`idProd`, `nom`, `description`, `prix`, `url_media`, `disponibilite`) VALUES
(1, 'Cr?me douceur esfoliante', 'Cr?me pour le visage,200ml', '32', 'medias/images/produits/1.jpg', 1),
(2, 'Lait d?maquillant douceur', 'Lait pour le visage,150ml', '18', 'medias/images/produits/2.jpg', 1),
(3, 'Gel lissant yeux & l?vres', 'Soins,15ml', '36', 'medias/images/produits/3.jpg', 1),
(4, 'Soin douceur jour', 'Soin,50ml', '34', 'medias/images/produits/4.jpg', 1),
(5, 'Cr?me douceur exfoliante', 'Cr?me pour le corp,200ml', '32', 'medias/images/produits/5.jpg', 1),
(6, 'Huile de beaut', 'Huile pour le corp,500ml', '66', 'medias/images/produits/6.jpg', 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `FK_commande_idClient` FOREIGN KEY (`idClient`) REFERENCES `clients` (`idClient`);

--
-- Contraintes pour la table `lignedecommande`
--
ALTER TABLE `lignedecommande`
  ADD CONSTRAINT `FK_ligneDeCommande_id` FOREIGN KEY (`idCommande`) REFERENCES `commande` (`idCommande`),
  ADD CONSTRAINT `FK_ligneDeCommande_idProd` FOREIGN KEY (`idProd`) REFERENCES `produits` (`idProd`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
