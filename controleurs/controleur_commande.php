<?php 

class Controleur_Commande extends Controleur{
	private $modele;

	public function __construct(){
		parent::__construct();
		$this->modele = new Commande();
	}
	
	public function ajouterCommande(){
		//Ajouter la commande dans la BD et récupérer son id
        $commande = $this->modele->ajouterCommande();
        //Insérer les lignes de la commande
        $ligneCommandes = $this->modele->ajouterLigneCommande($commande);
        //Calculer montant
        $montant = round($this->modele->calculerMontant($ligneCommandes), 2);
        //Calculer TPS
        $TPS = round($this->modele->calculerTPS($montant), 2);
        //Calculer TVQ
        $TVQ = round($this->modele->calculerTVQ($montant), 2);
        //Calculer livraison
        $livraison = $this->modele->calculerLivraison($montant);
        //Calculer prix total
        $total = round($this->modele->calculerTotal($montant, $livraison, $TPS, $TVQ), 2);
        //Mettre à jour la commande
        $this->modele->completerCommande($commande, $montant, $TPS, $TVQ, $livraison, $total);

        //Récupérer les données POST
		$a = $_POST;
		//D'uatres données néccessaires pour traiter le paiement:
		//L'adresse IP du client
		$IPADDRESS = $_SERVER['REMOTE_ADDR'];
		//Date d'expiration de la carte bleu
		$EXPDATE = $_POST['mois_paiement'] . $_POST['annee_paiement'];
		//Devises
		if($_POST["COUNTRYCODE"] == 'CA'){
			$CURRENCYCODE = 'CAD';
		}
		elseif ($_POST["COUNTRYCODE"] == 'USA'){
			$CURRENCYCODE = 'USD';
		}

		$b = array('IPADDRESS' => $IPADDRESS,
					'EXPDATE' => $EXPDATE,
					'CURRENCYCODE' => $CURRENCYCODE,
					'ITEMAMT' => $montant,
					'SHIPPINGAMT' => $livraison,
					'TAXAMT' => $TPS + $TVQ,
					'AMT' => $total,
					'DESC' => 'Description');
		//Fusionner toutes les données dans un tableau associatif
		$posted = array_merge($a, $b);

		//Créér un objet Gateway pour traiter le paiement
		$gateway = new Gateway();
		//Passer les données et l'id de la commande
		$msg = $gateway->process_paypal_transaction($posted, $commande);
		//Gateway retourne un objet PayPal
		$paypal = $gateway->feed_paypal_request($posted);
		//Mettre à jour le status de PayPal dans la BD, si le paiement a été bien traité 
		if($paypal->success){
			$this->modele->misajourStatus($commande);
			$this->vue->paypal($paypal->output_msg);
		}
		else {
			$this->vue->paypal($paypal->output_msg);
		}
	}

	
}
 ?>