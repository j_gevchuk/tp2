<?php 
/**
 * Fichier: 'controleur_panier.php'
 * * @author  Iulianna Gevchuk
 * Description: classe Controleur_Panier
 *
 */


	class Controleur_Panier extends Controleur {
		private $modele;

		public function __construct(){
			parent::__construct();
			//Ouvrir la session si ça n'et pas déjà faite
			if(!isset($_SESSION['panier'])){
				$_SESSION['panier'] = array();
			}
			//Créér un objet Panier
			$this->modele = new Panier();
		}

		//Fonction est appelée lorsque l'utilisateur clique 'Ajouter produit au panier'
		public function ajouter(){
			//Vérifier si l'id du produit existe dans la BD et s'il n'est pas encore ajouté au panier
			if($this->modele->verifProduit($_GET['ajouter'], 1)){
				$this->vue->ajouter($_GET['ajouter']);
			}
			//Le produit est déjà ajouté ou n'existe pas dans la BD et on affiche un message d'avertissement
			else {
				$this->vue->repetition();
			}
		}

		//Afficher le panier lorsque l'utilisateur clique sur le lien Panier
		public function gererPanier($action){
			switch ($action) {
				//Augmenter la quantité du produit
				case 'augmenter':
					$this->modele->augmenterQuantite($_GET['id']);
					$this->vue->quantite();
					break;
				//Diminuer la quantité du produit
				case 'diminuer':
					$this->modele->diminuerQuantite($_GET['id']);
					$this->vue->quantite();
					break;
				//Supprimer le produit du panier
				case 'supprimer':
					$this->modele->supprimerArticle($_GET['id']);
					if(count($_SESSION['panier']) < 1){
						header("Location:?page=accueil");
					}
					break;
				default:
					header("Location:?page=panier");
					break;
			}
		}

		//Vider le panier 
		public function viderPanier(){
			$this->modele->viderPanier();
			header("Location:?page=accueil");
		}
	}
 ?>