<?php 
/**
 * Fichier: 'main_controleur.php'
 * * @author  Iulianna Gevchuk
 * Description: classe Controleur
 *
 */

//  LE PROBLÈME QUI N'EST PAS RÉSOLUE: L'ACTION AJOUTER, SUPPRIMER, AUGMENTER ET DIMINUER LA QUANTITÉ SE FAIT LORSQUE ON CLIQUE UNE DEUXIÈME FOIS???!!!

    class Controleur {
        public $vue;
        public $url;

        public function __construct($url = ''){
            //Créér une instance Vues
            $this->vue = new Vues();
            $this->url = $url;
        }


        //Afficher un contenu selon le lien cliqué, j'ai modifié un peu les urls du menu...
        public function invoquer(){
            /*** Connexion sécurisée avec MD5 et le gran de sel ***/
            if(!isset($_SESSION["grainSel"]))
            {
                //générer le grain de sel côté serveur
                //nécéssaire pour obtenir md5(md5(password) + grainSel)
                $grainSel = rand(1, 1000);
                $_SESSION["grainSel"] = $grainSel;
            }

            //L'utilisateur clique sur 'Ajouter', 'Diminuer' ou 'Supprimer' (une article)
            if(isset($_GET['action']) && $_GET['action'] && $_GET['id']){
                $controleur = new Controleur_Panier();
                $controleur->gererPanier($_GET['action']);
            }

            //L'utilisateur clique sur 'Vider le panier'
            if(isset($_POST['emptyCart'])){
                $controleur = new Controleur_Panier();
                $controleur->viderPanier();
            }

            //Page 'index'
            $page;
            if($this->url == '' || !$this->url){
                $page = 'accueil';
            }
            //Tous autres cas
            else {
                $page = $this->url['page'];
            }

            switch($page){
            case 'accueil':
                $this->vue->accueil();
                break;
            case 'oliviers':
                $this->vue->oliviers();
                break;
            case 'produits':
                $modele = new Produits();
                //Récupérer tous les produits de la BD
                $produits = $modele->getListeProduits();
                //Afficher les produits dans la page
                $this->vue->produits($produits);
                if(isset($_GET['ajouter'])){
                    $controleur = new Controleur_Panier();
                    $controleur->ajouter();
                }
                break;
            case 'panier':
                $this->vue->panier();
                break;
            case 'contacts':
                $this->vue->contacts();
                break;
            case 'inscription':
                $controleur = new Controleur_Inscription();
                $controleur->enregistrer();
                break;
            case 'plan':
                $this->vue->planSite();
                break;
            case 'compte':
                $this->vue->compte();
                break;
            case 'paiement':
                // L'utilisateur n'est pas connecté
                if(!isset($_SESSION['user'])){
                    $this->vue->accueil();
                }
                else {
                    $this->vue->paiement();
                }
                break;
            case 'paypal':
                $controleur = new Controleur_Commande();
                $controleur->ajouterCommande();
                break;
                //Un autre url
            default: 
                $this->vue->accueil();
                break;
            }
        }
    }
 ?>