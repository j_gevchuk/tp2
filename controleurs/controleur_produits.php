<?php 
/**
 * Fichier: 'Controleur_Catalogue.php'
 * * @author Olga Zlotea
 * Création: 19.06.2013
 * 
 * Description:method appele a class Modele_Produits pour afficher les produits 
 */
include_once 'modeles/modele_produit.php';
	class Controleur_Produits extends Loader{

		public function produits(){
			$this->modele = new Modele_Produits();
		 	//Appeler la méthode qui récupère les produits de la BD
		 	$produits = $this->modele->getListeProduits();
		 	//Affciher dans un format HTML
		 	$vue = new Vues();
		 	$vue->produits($produits);
	 	}
	}
 ?>
