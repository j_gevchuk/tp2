<!DOCTYPE html 
 PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta charset='utf-8'/>
			<!-- Fonts et ASCII -->
			<?php
				//include_once("vues/fonts.htm");
			?>
			<title>Contact - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Contactez les Oliviers Ould Hocine, nous sommes à votres dispositions pour toute auestion commande ou recommandation sur l'huile d'olive et ses amis" />
			<meta name="keywords" content="contact, commande, questions, Olive, Huile, Algérie, Tradition, Ould Hocine, Bouira, Oliviers" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
			<!-- Appels CSS et javascript -->
			<?php
				include_once("vues/cssETjs.htm");
			?>
		</head>
<body class="corps">
	<div id="contenu">
		<!-- Entete -->
		<?php
			//include_once("vues/php/entete.php");
		?>
		<div id="contenuPrincipal">
			<!--Affichage le menu Admin-->
			<div class="menuAdmin">
				<?php
					include_once 'vues/session_admin.php';
					include_once 'vues/menuAdmin.htm';
				?>
				<!-- Afficher la liste de nouvelles commandes, payées et non -->
					<div id='traitement'></div>
					<table border="1" class="produit">
						<caption>Liste de nouvelles commandes</caption>
						<form action="" method="POST">
						<tr class="orange"><th>Commande</th><th>Date</th><th>Client</th><th>Montant</th><th>Status Paypal</th><th>Traitement</th></tr>
					<?php
					//Traiter les résultats récupérés de la BD comme des objets
					$commandes = $commandes->fetchAll();
					// Afficher les champs
					foreach ($commandes as $commande) :
					?>
						<tr>
							<td><?= $commande->idCommande ?></td>
							<td><?= $commande->dateCommande?></td>
							<td><?= $commande->prenom  .  ' ' . $commande->nom ?></td>
							<td><?= round($commande->montTotal, 2) ?>$</td>
							<td><?= $commande->statusPaypal ?></td>
							<td>
								<!-- Afficher les bouton 'checkbox' -->
								<input type="checkbox"  name="traitement[]" value="<?= $commande->idCommande ?>">
							</td>
						</tr>
					<?php  
					endforeach;
					?>
						<!-- Bouton 'Traiter' -->
						<tr><td><input type="submit" name="sbmTraitement" value="Traiter"/></td></tr>
						</form>
					</table>
			</div>
		</div>			
	</div>
</body>
</html>

