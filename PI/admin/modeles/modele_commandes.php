<?php 

class Modele_Commande {
	private $db;

	//Instancier un PDO 
	public function __construct(){
		$this->db = new AccesBD();
	}

	//Fonction récupère et affiche des produits de la base de données 
	public function getListeCommandes(){
		$connexion = $this->db->connecter();
		$requete = "SELECT c.idCommande, c.dateCommande, c.montTotal, c.statusPaypal, cl.courriel, cl.nom, cl.prenom FROM commande AS c LEFT JOIN clients AS cl ON cl.idClient = c.idClient WHERE c.statusTraitement = 0;";
		$resultats = $this->db->select($connexion, $requete);
		return $resultats;
	}

	//Fonction récupére les status de PayPal (payée ou non) et change le status du traitement
	public function traiterCommandes($status = null){
		//Aucun bouton n'est coché
		if($status == null){
			return false;
		}
		else {
			$connexion = $this->db->connecter();
			foreach ($status as $id) {
				$requete = "UPDATE commande SET statusTraitement = 1 WHERE idCommande = $id;";
				$this->db->preparer($connexion, $requete);
			}
			return true;
		}
	}
}
 ?>