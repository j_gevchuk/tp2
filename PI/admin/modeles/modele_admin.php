<?php 
/**
 * Fichier: 'modele_produit.php'
 * * @author Olga Zlotea
 * Création: 19.06.2013
 * 
 * Description: Chercher le produit 
 */
include_once('modeles/accessbd.php');
include_once('modeles/admin.php');
class Modele_Admin {
	private $db;
	//Instancier le modèle
	public function __construct(){
		$this->db = new AccesBD();
	}
	//Fonction qui compare le login et le mot de passe tappés avec ceux-ci dans la BD
	public function getAdmin($login,$pass){
		$connexion = $this->db->connecter();
		$requete = "SELECT * FROM admin WHERE emailAdmin='$login' AND passwordAdmin='$pass'";
		$resultats = $this->db->preparer($connexion, $requete);
		$resultat = $resultats->fetch(PDO::FETCH_ASSOC);
		//Si la requête mysql retourne 1 ligne, on ouvre la session pour l'admin
		if($resultat){
			$_SESSION['admin'] = $resultat;
		}
	}
}
 ?>
