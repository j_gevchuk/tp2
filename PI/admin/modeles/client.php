<?php 
class Client {
	private $idClient;
	private $courriel;
	private $password;
	private $nom;
	private $prenom;	
	private $telephone;
	

		public function __construct($idClient = "",$courriel, $password,$nom,$prenom, $telephone=""){
			$this->idClient = $idClient;
			$this->courriel = $courriel;
			$this->password = $password;
			$this->nom = $nom;
			$this->prenom = $prenom;
			$this->telephone = $telephone;
		}

		public function getIdClient(){
			return $this->idClient;
		}
		
		public function getCourriel(){
			return $this->courriel;
		}
		public function setCourriel($courriel){
			$this->courriel = $courriel;
		}
		public function getPassword(){
			return $this->password;
		}
		public function setPassword($password){
			$this->password = $password;
		}
		public function getNom(){
			return $this->nom;
		}
		public function setNom($nom){
			$this->nom = $nom;
		}
		public function getPrenom(){
			return $this->prenom;
		}
		public function setPrenom($prenom){
			$this->prenom = $prenom;
		}
		public function getTelephone(){
			return $this->telephone;
		}
		public function setTelephone($telephone){
			$this->telephone = $telephone;
		}
	
}
 ?>