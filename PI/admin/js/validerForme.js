﻿		/**
		 * @author Lilia Ould Hocine
		 * @version 1.0 Automne 2011
		 */

		/**
		 * 
		 * @param {Object} aElements
		 * @return {Boolean} true si tous les zones de texte du formulaire sont saisies
		 * false dans tous les autres cas
		 */
		function validerForm(aElements)
		{
			//La variable du résultat
			var bResultat = true;	
			
			//Parcourir tous les élements du formulaire (Pas de fieldset)
			for(var iElem = 0; iElem < aElements.length ; iElem++)	
			{	
				var sType = aElements[iElem].type; //Variable pour le type de l'element
				var erreur = "err_" + aElements[iElem].name; //récupérer l'id de la div d'erreur
				var label = "lbl_" + aElements[iElem].name; //récupérer l'id du label
				var erreurText = document.getElementById(erreur); //récupérer l'element de qui contienra le message d'erreur
				var labelText = document.getElementById(label); //récupérer le label
				
				//Faire un switch sur les différents types des elements du formulaire
				switch (sType) 
				{
					case "text" :
					{
						////Si l'élelement est de type text
						if ((aElements[iElem].value == "") && (aElements[iElem].disabled == false))
						{
							//Le cas ou la valeur est vide
							labelText.style.fontWeight="bold";
							labelText.style.color="red";
							erreurText.innerHTML = "*";
							aElements[iElem].focus();
							bResultat = false;
						}
						else if ((aElements[iElem].value != "") && (aElements[iElem].disabled == false))
						{
							//Le cas ou la valeur n'est vide
							labelText.style.fontWeight="normal";
							labelText.style.color="#1B3613";
							erreurText.innerHTML = "";
							var leType = aElements[iElem].name.charAt(3);
								if (leType == "n")
								{
									//Le cas ou la valeur est un nombre
									if (isNaN(aElements[iElem].value))
									{
										labelText.style.fontWeight="bold";
										labelText.style.color="red";
										erreurText.innerHTML = "Ce champ doit &ecirc;tre de type num&eacute;rique, ex. : 3800.";
										bResultat = false;
										aElements[iElem].focus();
										
									} else {
											labelText.style.fontWeight="normal";
											labelText.style.color="#1B3613";
											erreurText.innerHTML = "";	
									}
								} else if (leType == "s")
								{	
									//Le cas ou la valeur est une chaine de caractère
									if(!isNaN(aElements[iElem].value))
									{
										labelText.style.fontWeight="bold";
										labelText.style.color="red";
										erreurText.innerHTML = "Vous devez entrer une chaine de caract&egrave;res";
										bResultat = false;
										aElements[iElem].focus();
										
									} else {
										labelText.style.fontWeight="normal";
										labelText.style.color="#1B3613";
										erreurText.innerHTML = "";	
									}
								} else if (leType == "c")
								{	
									var codeReg = /^\w+\@\w+\.\w+$/;
									if(codeReg.test(aElements[iElem].value))
									{
										labelText.style.fontWeight="normal";
										labelText.style.color="#1B3613";
										erreurText.innerHTML = "";											
									} else {										
										labelText.style.fontWeight="bold";
										labelText.style.color="red";
										erreurText.innerHTML = "Ce courriel est erroné!";
										bResultat = false;
										aElements[iElem].focus();										
									}
								} else if (leType == "p")
								{
									var codeReg = /^[0-9][0-9].[0-9][0-9]$/;
									if(codeReg.test(aElements[iElem].value))
									{
										labelText.style.fontWeight="normal";
										labelText.style.color="#1B3613";
										erreurText.innerHTML = "";											
									} else {										
										labelText.style.fontWeight="bold";
										labelText.style.color="red";
										erreurText.innerHTML = "Le prix doit être de la forme 99.99!";
										bResultat = false;
										aElements[iElem].focus();										
									}
								}
						}
						break;
					} 
					case "select-multiple" :
					{
						//Si l'élelement est une liste multiple
						bResultat = (validerListeMultiple(aElements[iElem]) && bResultat);
						break;
					} 
					case "select-one" :
					{
						//Si l'élelement est une liste simple
						bResultat = (validerListeSimple(aElements[iElem]) && bResultat);
						break;
					}
					case "password" :
					{
						////Si l'élelement est de type password
						if ((aElements[iElem].value == "") && (aElements[iElem].disabled == false))
						{
							//Le cas ou la valeur est vide
							labelText.style.fontWeight="bold";
							labelText.style.color="red";
							erreurText.innerHTML = "*";
							aElements[iElem].focus();
							bResultat = false;
						} else {
							labelText.style.fontWeight="normal";
							labelText.style.color="#1B3613";
							erreurText.innerHTML = "";											
						}
					}
					case "textarea" :
					{
						////Si l'élelement est de type textarea
						if ((aElements[iElem].value == "") && (aElements[iElem].disabled == false))
						{
							//Le cas ou la valeur est vide
							labelText.style.fontWeight="bold";
							labelText.style.color="red";
							erreurText.innerHTML = "*";
							aElements[iElem].focus();
							bResultat = false;
						} else {
							labelText.style.fontWeight="normal";
							labelText.style.color="#1B3613";
							erreurText.innerHTML = "";											
						}
					}
					case "file" :
					{
						////Si l'élelement est de type textarea
						if ((aElements[iElem].value == "") && (aElements[iElem].disabled == false))
						{
							//Le cas ou la valeur est vide
							labelText.style.fontWeight="bold";
							labelText.style.color="red";
							erreurText.innerHTML = "*";
							aElements[iElem].focus();
							bResultat = false;
						} else {
							labelText.style.fontWeight="normal";
							labelText.style.color="#1B3613";
							erreurText.innerHTML = "";											
						}
					}
				}
			
			}
		return bResultat;
	}
			
		
		
		/**
		 * @description Comme le bouton de r&eacute;initialisation ne touche pas aux division
		 * J'ai rajout&eacute; cette proc&eacute;dure pour le faire
		 * @param (aElement) le tableaux des &eacute;lements du formulaire
		 */
		function mettreEnNoir(aElements)
		{
			/* D&eacute;clarations */
			var sIdLbl;
			var iElem;
			var sIdErr;
			//Pour tous les &eacute;l&eacute;ments du tableau des inputs du formulaire    
			for (iElem = 0;iElem < aElements.length;iElem++) {
							
					if (aElements[iElem].type == "text") {	
						if (aElements[iElem].disabled == false) {
						sIdLbl = "lbl_" + aElements[iElem].name;
						sIdErr = "err_" + aElements[iElem].name;			
						document.getElementById(sIdErr).innerHTML = ""; //valeur par d&eacute;faut 
						document.getElementById(sIdLbl).style.color = "#1B3613";
						document.getElementById(sIdLbl).style.fontWeight="normal";
						} //fin de if
					}// fin de if
			}//fin de for 
		}// fin de mettre en noir appell&eacute;e par le boutton r&eacute;initialiser
		
		/* Valider les liste multiples */
		
		function validerListeMultiple(oSelect)
		{
			var bValide = true;
			var sIdErr = "err_" + oSelect.name;
			var sIdLbl = "lbl_" + oSelect.name;
			var nb = 0;
			
			while ((nb != oSelect.options.length) && (!oSelect.options[nb].selected))
			{
				nb++;
			}
			
			if(nb == oSelect.options.length)
			{
				document.getElementById(sIdLbl).style.fontWeight="bold";
				document.getElementById(sIdLbl).style.color="red";
				document.getElementById(sIdErr).innerHTML = "Vous devez faire une selection";
				bValide = false;
			} else 
			{
				document.getElementById(sIdLbl).style.fontWeight="normal";
				document.getElementById(sIdLbl).style.color="#1B3613";
				document.getElementById(sIdErr).innerHTML = "";
			}
			return bValide;
		}
		
		/* Valider les liste simples */
		function validerListeSimple(oSelect)
		{
			var bValide = true;
			var sIdErr = "err_" + oSelect.name;
			var sIdLbl = "lbl_" + oSelect.name;
			
			if(oSelect.selectedIndex == 0)
			{
				document.getElementById(sIdLbl).style.fontWeight="bold";
				document.getElementById(sIdLbl).style.color="red";
				document.getElementById(sIdErr).innerHTML = "*";
				bValide = false;
			} else 
			{
				document.getElementById(sIdLbl).style.fontWeight="normal";
				document.getElementById(sIdLbl).style.color="#1B3613";
				document.getElementById(sIdErr).innerHTML = "";
			}
			return bValide;
		}