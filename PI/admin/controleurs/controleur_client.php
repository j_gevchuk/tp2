<?php 

include_once 'modeles/modele_client.php';
	//Class controleur de produits
	class Controleur_Client{
		private $client;
		//Method aficher les clients de bd
		public function client(){
			$this->modele = new Modele_Client();
		 	//Appeler la méthode qui récupère les clients de la BD
		 	$clients = $this->modele->getListeClients();
		 	//Affciher dans un format HTML
		 	$vue = new Vues();
		 	$vue->clients($clients);
	 	}
	 	//Method supprimer un client de bd
	 	public function supprimerClient(){
	 		$this->modele = new Modele_Client();
		 	//Appeler la méthode qui récupère les produits de la BD
		 	$res = $this->modele->supprClient($_POST['id']);
		 	if($res){
		 		$clients =$this->modele->getListeClients();
		 		$vue = new Vues();
		 		$vue->clients($clients);
		 	}
	 	}
	 	//Method select un produit de bd avec IdProduit
	 	public function selectProduit(){
	 		$this->modele = new Modele_Produits();
			$id = $_GET['idProd'];
			$produit = $this->modele->getProduit($id);
			$vue = new Vues();
			$vue->formProduits($produit);
		}
		//Method modifier un produit
		public function modifierProd(){
			$this->modele = new Modele_Produits();
			$vue = new Vues();
			$vue->formModProd();
			if($_POST['enregistrer']) {
				$id = $_POST['txtsId'];
				$nom = $_POST['txtsLogin'];
				$description = $_POST['txtsDescr'];
				$prix = $_POST['txtsPrix'];
				$url_media = $_POST['txtsUrl'];
				$disponible = $_POST['txtsDisp'];
				$res = $this->modele->modifierProduit($id,$nom,$description, $prix,$url_media, $disponible);
				if($res){
					$produits =$this->modele->getListeProduits();
			 		$vue = new Vues();
			 		$vue->produits($produits);
				}
			}
		}

		//Method qui ajout un produit à la bd
		public function ajouterProduit(){
			$this->modele = new Modele_Produits();
			if($_POST['enregistrerProd']){
				$nom = $_POST['txtsLogin'];
				$description = $_POST['txtsDescr'];
				$prix = $_POST['txtsPrix'];
				$url_media = $_POST['txtsUrl'];
				$disponibilite = $_POST['txtsDisp'];
			}
			$produit = $this->modele->ajouterProduit($nom, $description,$prix,$url_media,$disponibilite);
			if($produit){
				$produits = $this->modele->getListeProduits();
				$vue = new Vues();
				$vue->produits($produits);
			}
		}

	}
 ?>
