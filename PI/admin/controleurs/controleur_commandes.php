<?php 

class Controleur_Commandes {
	private $modele;
	private $vue;

	public function __construct(){
		$this->modele = new Modele_Commande();
		$this->vue = new Vues();
	}

	//Method afiche les commandes de la BD
	public function listeCommandes(){
	 	//Appeler la méthode qui récupère les produits de la BD
	 	$commandes = $this->modele->getListeCommandes();
	 	//Affcher dans un format HTML
	 	$this->vue->commandes($commandes);
 	}

	 //Method permettant de traiter les commandes (changer le status du traitement de 0 à 1)
 	public function traiterCommandes(){
		$ids = array();
		//Récupérer les valeurs (id) des boutons cochés dans un tableau
		foreach ($_POST['traitement'] as $id) {
			$ids[] = $id;
		}
		//Passer les valeurs au modèle
		if($this->modele->traiterCommandes($ids)){
			$this->vue->traitement();
		}
 	}
}
 ?>