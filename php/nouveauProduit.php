<!-- Formulaire ppour ajouter un nouveau produit -->
 	<h2>Ajouter un nouveau produit!</h1>
   	<div id="nouveauProduit"><form method="post" name="nouveauProduit" enctype="multipart/form-data" action="" >					
					
		<div class="zone">
			<label id="lbl_txtsTitre">Nom du produit : </label>
			<input type="text" name="txtsTitre" >		
			<div class="err" id="err_txtsTitre"></div>
		</div>
		
		<div class="zone">
			<label id="lbl_article">Description : </label>
			<textarea name="article" rows="20" cols="40" ></textarea>					
			<div class="errConnexion" id="err_article"></div>
		</div>
		
		<div class="zone">
			<label id="lbl_txtpPrix">Prix : </label>
			<input type="text" name="txtpPrix" >						
			<div class="err" id="err_txtpPrix"></div>
		</div>
		
		<div class="zone">
				<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $poids_max; ?>">
			
			
			<label id="lbl_fichier">Fichier : </label>
			<input type="file" size="24" name="fichier">
			<div class="errConnexion" id="err_fichier"></div>
			
			<div class="vide"></div>
			<input type="button" value="Envoyer" name="envoyer" onclick="if (validerForm(nouveauProduit.elements)) nouveauProduit.submit();">
		</div>
			
    </form></div>
	<div><h2 style="padding-top:70px;">Disponibilité des produits!</h1>
	<?php
		//Inclure la apage pour gérer les produits éxistants
		include_once("gererProduits.php");
	?>
