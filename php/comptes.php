<?php
	//Si l'utilisateur est connecté on accède au compte sinon on renvoit vers le formulaire d'inscription
	if(isset($_SESSION["user"]))
	{
		//Connexion à la base de données
		laConnexion();
		
		//Récupérer l'ID du client
		$clientID = $_SESSION["user"];
		
		//Récupérer les informaton sur ses transictions
		$requete = "select commande.id as IDcomm, clients.telephone as telClient, clients.pays as paysClient, clients.courriel as courrielClient,commande.date_com as dateComm, produits.nom as nomProduit, produits.prix as prixProduit, commande_produits.qte as qteProduit from clients inner join commande on commande.id_client=clients.id inner join commande_produits on commande_produits.id_commande=commande.id inner join produits on produits.id=commande_produits.id_produits where clients.id=" . $clientID . " ORDER BY commande.id DESC";
		$historique = laSelection($requete);
		$historiqueBis = laSelection($requete);
		
		//S'il y a un retour c'est qu'il y a un article sinon on revient àa la page articles
		if($rangee = mysql_fetch_array($historiqueBis))
		{
			//Si on trouve des commandes on commence à construire le html, en commançant par exposer au client ses information les plus importantes
			// A savoir le pays et le téléphone qui vont permettre de faire le suivi de ses commandes
			echo "<div class='paragraphe'>";
			?> 
			<h1>Votre compte</h1>
			<h4 style="font-weight:normal; padding-bottom:0;"><span style="font-weight:bold;">Courriel: </span><?php echo $rangee["courrielClient"]; ?>.</h4> 
			<h4 style="font-weight:normal; padding:0;"><span style="font-weight:bold;">Pays : </span><?php echo $rangee["paysClient"]; ?>.</h4> 
			<h4 style="font-weight:normal; padding-top:0;"><span style="font-weight:bold;">Téléphone : </span><?php echo $rangee["telClient"]; ?>.</h4> 
			<?php	
				
			$i=0; //Le numéro de l'article
			$comm =""; //Le numéro de la commande
			$total = 0; //Le total de l'achat
			while($rangee = mysql_fetch_array($historique))
			{
				//Construire la liste des commandes
				$i++;
				if($comm != $rangee["IDcomm"])
				{
					//Si la commande change, on affiche les détails de la nouvelle commande, mais avant on affiche le total de la commande précédente
					if(($comm != $rangee["IDcomm"]) && ($comm != ""))
					{
						?><h4 style="text-align:center; color:#35478C; padding-top:30px">Total = <?php echo round($total, 2) ?>$</h4><?php
						$total = 0;
					}
					$comm =$rangee["IDcomm"];
					echo "<div class='panierAffiche'>";
					?> <hr /><h4 style="color:#35478C;">Commande du  <?php echo $rangee["dateComm"]; ?> :</h4> <?php
					echo "<ul>";
						echo "<li style='font-weight:bold;'>N°</li>";
						echo "<li style='font-weight:bold;'>Produit </li>";
						echo "<li style='font-weight:bold;'>Quantité </li>";
						echo "<li style='font-weight:bold;'>Prix </li>";
						echo "<li style='font-weight:bold;'>Total </li>";
					echo "</ul>";
					echo "</div>";
					$i = 1;
				} 
				echo "<div class='panierAffiche'>";
				echo "<ul>";
					echo "<li>" . $i . ")</li>";
					echo "<li>". $rangee["nomProduit"]  ."</li>";
					echo "<li>". $rangee["qteProduit"] ."</li>";
					echo "<li>". $rangee["prixProduit"] ."$</li>";
					echo "<li>". round((intval($rangee["qteProduit"]) * floatval($rangee["prixProduit"])), 2) ."$</li>";
					$total = $total + (intval($rangee["qteProduit"]) * floatval($rangee["prixProduit"]));
				echo "</ul>";
				echo "</div>";
				
			}
				//On affiche le total pour la dernière commande
				?><h4 style="text-align:center; color:#35478C; padding-top:30px">Total = <?php echo round($total, 2) ?>$</h4><?php
			echo "</div>";
		} else {
			//Si le client n'a passé aucune commande
			?> <h4 style="text-align:center;">Vous n'avez encore passé aucune commande!</h4> <?php		
		}
		
		
	} else {
		//Si le client n'est pas connecté
		header("Location: Fiche_Oho.php");
	}
?>