<?php 
/**
 * Fichier: 'vues.php'
 * * @author  Iulianna Gevchuk
 * Description: Vues
 *
 */

	class Vues {
		public function accueil() {
			include_once('vues/Accueil.php');
		}

		public function oliviers() {
			include_once('vues/Olive_Oho.php');
		}

		public function contacts() {
			include_once('vues/Contacts_Oho.php');
		}

		public function planSite() {
			include_once('vues/plan.php');
		}

		public function produits($produits){
			include_once('vues/Produits_Oho.php');
		}

		public function panier(){
			include_once 'vues/panier.php';
		}

		public function compte(){
			include_once 'vues/Fiche_Oho.php';
		}

		public function ajouter(){
			include_once 'vues/ajout.php';
		}

		public function repetition(){
			include_once 'vues/repetition.html';
		}

		public function paiement(){
			include_once 'vues/paiement.php';
		}

		public function quantite(){
			if(isset($_GET['augmenter'])){
				include_once 'vues/augmenter.php';
			}
			elseif (isset($_GET['diminuer'])) {
				include_once 'vues/diminuer.php';
			}
		}

		public function paypal($msg){
			include_once 'vues/paypal.php';
		}
	}

 ?>