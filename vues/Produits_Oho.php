<?php ob_start(); ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../css/css.css" rel="stylesheet" type="text/css" />
		
		<head>
			<!-- Fonts et ASCII -->
			<?php
				include_once("php/fonts.htm");
			?>
			<title>Nos produits - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Produits d'huile d'olive Algérienne, une huile d'olive savoureuse et fruitée, le choix entre plusieurs produits de diverses methodes de pression" />
			<meta name="keywords" content="Commande, acheter, produits, Kabylie, Olive, Huile, Algérie, Tradition, Ould Hocine, Bouira, Oliviers" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("php/cssETjs.htm");
			?>
		</head>
		
<body class="corps">
	<div id="contenu">
		
		<!-- Entete -->
		<?php
			include_once("php/entete.php");
		?>
		
		
		<!-- Le menu -->
		<?php
			include_once("php/menu.htm");
		?>		
		
		<!-- Contenu pprincipal -->
		<div id="contenuPrincipal">	
			<?php

			// 	foreach ($produits as $obj) {
			// 	print_r($produit->getNom());
			// }
				include_once("produits.php");
			?>
		</div>		
		
		<!-- Bas de page -->
		<?php
			include_once("php/reseaux.htm");
			include_once("php/piedPage.htm");
		?>			
	</div>
	
	<?php
		include_once("php/jQuery.htm");
	?>
	<script  type="text/javascript">
		// <![CDATA[
			menuProduits();
		// ]]>
	</script>
</body>
</html>
<?php ob_flush(); ?>