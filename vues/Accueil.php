<?php ob_start(); ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- CSS -->
		
		<head>
			<!-- Fonts et ASCII -->
			<?php
				include_once("php/fonts.htm");
			?>
			<title>Accueil - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Bienvenue sur le site web consacré aux oliviers Ould Hocine, ce site web consite à mettre en valeur les produits venu droits des terres Kabyle ou l'olivier et l'huile d'olive sont plus qu'une tradition, ils sont le synonyme de la vie" />
			<meta name="keywords" content="Bienvenue, Olive, Huile, OHO, cuisine, savoir vivre, rafinement, fine bouche, méditérranée" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("php/cssETjs.htm");
			?>
			<!--liens menant vers les scripts CSS et javascript-->
			<script src="js/swfobject_modified.js" type="text/javascript" ></script>
		</head>
		
<body class="corps">
	<div id="contenu">
		
		
		<!-- Entete -->
		<?php
			include_once("php/entete.php");
		?>
		
		
		<!-- Le menu -->
		<?php
			include_once("php/menu.htm");
		?>		
		
		<!-- Contenu pprincipal -->
		<div id="contenuPrincipal">	
			<div id="contenuCentre">
				<h1>Les Oliviers Ould Hocine</h1>
				<p>Dans les multiples saveurs de nos huiles d’olive se reflètent un arbre, l’olivier, un terroir et un savoir-faire…</p>
				<p>Ici, en Kabylie, l’olivier est implanté depuis des siècles avec la variété d’olive « Aglandau ». Ce nom provençal signifie « en forme de gland ». C’est une olive réputée pour son huile onctueuse, au fruité intense et à l’excellente conservation.</p>

				<p>L'Algéie, c’est ce beau pays méditerranéen, où les oliveraies épousent les collines bercées par le mistral, ce vent qui toujours fait revenir le soleil. Les sols calcaires et bien drainés offrent à l’olivier un terroir idéal.</p>
				<p>Le savoir-faire, c’est notre culture familiale et kabyle, la transmission du travail de l’olivier et des secrets de l’huile d’olive de génération en génération.</p>	
			</div>
			<?php
			include_once("php/flash.htm");
			?>
		</div>		
		
		<!-- Bas de page -->
		<?php
			include_once("php/reseaux.htm");
			include_once("php/piedPage.htm");
		?>		
	</div>
	
	<?php
		include_once("php/jQuery.htm");
	?>
	<script  type="text/javascript">
		// <![CDATA[
			menuAccueil();
		// ]]>
	</script>
	
</body>
</html>
<?php ob_end_flush(); ?> 