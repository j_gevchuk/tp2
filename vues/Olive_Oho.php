<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml">

		
		<head>
			<!-- Fonts et ASCII -->
			<?php
				include_once("php/fonts.htm");
			?>
			<title>Olive - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Qui sont les Ould Hocine, et quelle est l'histoire de leurs Oliviers, comment procédent ils pour fabriquer leur huile et quelle leur relation avec cette tradition ancestrale" />
			<meta name="keywords" content="Olive, Oliviers, histoire, Brakka, El Hachimiya" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("php/cssETjs.htm");
			?>
		</head>
		
<body class="corps">
	<div id="contenu">
		
		<!-- Entete -->
		<?php
			include_once("php/entete.php");
		?>
		
		
		<!-- Le menu -->
		<?php
			include_once("php/menu.htm");
		?>		
		
		<!-- Contenu pprincipal -->
		<div id="contenuPrincipal">	
			<?php
				include_once("php/aproposdenous.htm");
			?>
		</div>		
		
		<!-- Bas de page -->
		<?php		
			include_once("php/reseaux.htm");
			include_once("php/piedPage.htm");
		?>		
	</div>
	
	<?php
		include_once("php/jQuery.htm");
		include_once("php/jQueryCycle.htm");
	?>
	<script  type="text/javascript">
		// <![CDATA[
			menuOliviers();
		// ]]>
	</script>
</body>
</html>