<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- CSS -->
		
		<head>
			<!-- Fonts et ASCII -->
			
			<?php
				include_once("php/fonts.htm");
				error_reporting(0);
			?>
			<title>Accueil - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Bienvenue sur le site web consacré aux oliviers Ould Hocine, ce site web consite à mettre en valeur les produits venu droits des terres Kabyle ou l'olivier et l'huile d'olive sont plus qu'une tradition, ils sont le synonyme de la vie" />
			<meta name="keywords" content="Bienvenue, Olive, Huile, OHO, cuisine, savoir vivre, rafinement, fine bouche, méditérranée" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("php/cssETjs.htm");
			?>
			<!--liens menant vers les scripts CSS et javascript-->
			<script src="js/swfobject_modified.js" type="text/javascript" ></script>
		</head>

<body class="corps">
	<?php ob_start(); ?>	
	<div id="contenu">
		
		
		<!-- Entete -->
		<?php
			include_once("php/entete.php");
		?>
		
		
		<!-- Le menu -->
		<?php
			include_once("php/menu.htm");
		?>		
		<!-- Contenu pprincipal -->
		<div id="contenuPrincipal">
			<div id="contenuCentre">	
				<form method="post" id="frm1" action="?page=paypal" name="paiement"  class='paiement'>
				<ul>
				<li><label for="address" class='thick'>Votre adresse</label></li>
				<li>
					<label for="street" id="rue">Rue : </label>
					<input type="text" id="STREET" name="STREET" class='rfield' value=""/>
					<div class='msgError'></div>
				</li>
				<li>
					<label for="city" id="ville">Ville : </label>
					<input type="text" id="CITY" name="CITY" class='rfield' value=""/>
					<div class='msgError'></div>
				</li>
				<li><label for="province" id="province">Province : </label>
					<input type="text" id="STATE" name="STATE" class='rfield' value=""/>
					<div class='msgError'></div>
				</li>
				<li><label for="pays">Pays : </label>
					<select name="COUNTRYCODE">
					<option value="CA">Canada</option>
					<option value="US">États-Unis</option>
					</select>
				</li>
				<li><label for="zip">Code postal : </label>
					<input type="text" id="ZIP" name="ZIP" class='rfield' value=""/>
					<div class='msgError'></div>
				</li>
				<li><label for="cart" class='thick'>Les informations de votre carte bancaire</label>
				<li><label for="surname" id="nom">Nom : </label>
					<input type="text" id="LASTNAME" name="LASTNAME" class='rfield' value=""/>
					<div class='msgError'></div>
				</li>
				<li><label for="name" >Prénom : </label>
					<input type="text" id="FIRSTNAME" name="FIRSTNAME" class='rfield' value=""/>
					<div class='msgError'></div>
				</li>
				<li><label for="type">Type de carte : </label>
					<select name="CREDITCARDTYPE">
					<option value="Visa">Visa</option>
					<option value="MasterCard">Master Card</option>
					</select>
				</li>
				<li><label for="number">Numéro de carte : </label>
					<input type="text" id="ACCT" name="ACCT" class='rfield' maxlength='19' value=""/>
					<div class='msgError'></div>
				</li>
				<li><label for="year">Année d'éxpiration : </label>
					<select name="annee_paiement">
					<option value="2013">2013</option>
					<option value="2014">2014</option>
					<option value="2015">2015</option>
					<option value="2016">2016</option>
					<option value="2017">2017</option>
					<option value="2018">2018</option>
					<option value="2019">2019</option>
					<option value="2020">2020</option>
					<option value="2021">2021</option>
					<option value="2022">2022</option>
					</select>
				</li>
				<li><label for="month">Mois D'éxpiration : </label>
					<select name="mois_paiement">
					<option value="01">1</option>
					<option value="02">2</option>
					<option value="03">3</option>
					<option value="04">4</option>
					<option value="05">5</option>
					<option value="06">6</option>
					<option value="07">7</option>
					<option value="08">8</option>
					<option value="09">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					</select>
				</li>
				<li><label for="cvv">Numéro de sécurité <small>à trois chiffres</small> : </label> 
					<input type="text" id="CVV2" name="CVV2" class='rfield' value="" maxlength='3'/>
					<div class='msgError'></div>
				</li>
				<li><input type="button" name="paiement_panier" value="Confirmer paiement">
					<input type="hidden" name='hiddenPaiement'/>
				</ul>
			</form>
			</div>
		</div>

		<!-- Bas de page -->
		<?php
			include_once("php/reseaux.htm");
			include_once("php/piedPage.htm");
		?>		
	</div>
	
	<?php
		include_once("php/jQuery.htm");
	?>
	<script  type="text/javascript">

		// <![CDATA[
			menuAccueil();
		// ]]>
	</script>
		<script src='js/validPaiement.js'></script>

	<?php ob_end_flush(); ?>
	
</body>
</html>




	
		
