<?php
ob_start();
//Afficher tous les produits récupérés de la base de données
	foreach ($produits as $produit) {
?>
		<div class='article'>
			<span class='img'><image src="<?= $produit->url_media ?>" alt=""></span>
			<input type="hidden" value="<?= $produit->idProd ?>"/>
			<span class='nom'><?= $produit->nom ?></span><br/>
			<span class='description'><?= $produit->description ?></span>
			<span class='prix'><?= number_format($produit->prix, 2, '.', '') ?>$</span>
			<span class='actions'><span class='ajouter'><a href="?page=produits&ajouter=<?= $produit->idProd ?>"><img src="medias/icons/buy.ico" alt="" />
				</a></span></span>
		</div>
<?php
	}
ob_end_flush();
?>


