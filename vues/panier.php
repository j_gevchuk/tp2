<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- CSS -->
		
		<head>
			<!-- Fonts et ASCII -->
			<?php
				include_once("php/fonts.htm");
				error_reporting(0);
			?>
			<title>Accueil - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Bienvenue sur le site web consacré aux oliviers Ould Hocine, ce site web consite à mettre en valeur les produits venu droits des terres Kabyle ou l'olivier et l'huile d'olive sont plus qu'une tradition, ils sont le synonyme de la vie" />
			<meta name="keywords" content="Bienvenue, Olive, Huile, OHO, cuisine, savoir vivre, rafinement, fine bouche, méditérranée" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("php/cssETjs.htm");
			?>
			<!--liens menant vers les scripts CSS et javascript-->
			<script src="js/swfobject_modified.js" type="text/javascript" ></script>
		</head>

<body class="corps">
	<?php ob_start(); ?>	
	<div id="contenu">
		
		
		<!-- Entete -->
		<?php
			include_once("php/entete.php");
		?>
		
		
		<!-- Le menu -->
		<?php
			include_once("php/menu.htm");
		?>		
		
		<!-- Contenu pprincipal -->
		<div id="contenuPrincipal">
			<div id="contenuCentre">	
				<div class='not-login'></div>
				<?php 
				 ?>
		<?php 
		// Si le panier ne contient pas d'articles, on ne peut pas y accéder
		if(count($_SESSION['panier']) < 1){
			return;
		}


		//Le prix total
			$total = 0;
			//Afficher le contenu du panier 
			?>
			<form action="?page=paiement" method='post'>
				<input type="hidden" id="idUser" name="idMembre" value="<?php echo $_SESSION["user"]; ?>"/>
			<?php 

					foreach ($_SESSION['panier'] as $key => $value): ?>

						<div class='article'>
						<span class='img'><image src='<?= $value['url_media'] ?>' alt='<?= $value['nom'] ?>'></span>
						<input type="hidden" name="idProd" value="<?= $value['idProd'] ?>">
						<span class='nom'><?= $value['nom'] ?></span><br/>
						<span class='description'><?= $value['description'] ?></span><br/>
						<!-- Afficher le prix de chaque article multiplié par quantité -->
						<span class='prix'><?= number_format(($value['prix'] * $value['quantite']), 2) ?>$</span>
						<span class='actions'>
							<!-- Afficher l'icône 'Diminuer la quantité' -->
							<a href="index.php?page=panier&action=diminuer&id=<?= $key ?>">
								<img src="medias/icons/minus.gif" alt="" />
							</a>
							<span class='quantite'><?= $value['quantite'] ?></span>
							<!-- Afficher l'icône 'Augmenter la quantité' -->
							<a href="index.php?page=panier&action=augmenter&id=<?= $key ?>">
								<img src="medias/icons/plus.gif" alt="" /></a>
							<!-- Afficher l'icône 'Supprimer' -->
							<span class='supprimer'>
								<a href="index.php?page=panier&action=supprimer&id=<?= $key ?>" class='supprimer'><img src="medias/icons/bin.png" alt="" />
								
								</a>
							</span>
						</span>
						</div>
					<?php endforeach;  
					// Prix total 
					$total += $value['prix'] * $value['quantite']?>
						<div class='total'><span>Prix hors taxes : <?= number_format($total, 2) ?>$</span><br/>
							<input type="hidden" name="prix" value="<?= number_format($total, 2) ?>">
							<span>TVA : <?= number_format(($total * 0.145), 2) ?>$</span><br/>
							<input type="hidden" name="tva" value="<?= number_format(($total * 0.145), 2) ?>">
							<span>Total : <?= number_format(($total * 1.145), 2) ?>$</span>
							<input type="hidden" name="prixTotal" value="<?= number_format(($total * 1.145), 2) ?>">
						</div>
						<div class="order">
							<input type="hidden" name='user' value='<?= $_SESSION['user']?>'/>
							<input type="submit" name='commande' value='Commander' onclick='checkConnection(e);'/>
						</div>
				</form>
				<form method="POST">
					<div class='empty'>
						<input type="submit" name='emptyCart' value='Vider le panier'/>
					</div>
				</form>
				<div class='notConnected'></div>
			</div>
		</div>

		<!-- Bas de page -->
		<?php
			include_once("php/reseaux.htm");
			include_once("php/piedPage.htm");
		?>		
	</div>
	
	<?php
		include_once("php/jQuery.htm");
	?>
	<script  type="text/javascript">
		// <![CDATA[
			menuAccueil();
		// ]]>
	</script>
	<?php ob_end_flush(); ?>
</body>
</html>





	
		
