<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml">

		
		<head>
			<!-- Fonts et ASCII -->
			<?php
				include_once("php/fonts.htm");
			?>
			<title>Contact - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Contactez les Oliviers Ould Hocine, nous sommes à votres dispositions pour toute auestion commande ou recommandation sur l'huile d'olive et ses amis" />
			<meta name="keywords" content="contact, commande, questions, Olive, Huile, Algérie, Tradition, Ould Hocine, Bouira, Oliviers" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("php/cssETjs.htm");
			?>
		</head>
		
<body class="corps">
	<div id="contenu">
		
		<!-- Entete -->
		<?php
			include_once("php/entete.php");
		?>
		
		
		<!-- Le menu -->
		<?php
			include_once("php/menu.htm");
		?>		
		
		<!-- Contenu pprincipal -->
		<div id="contenuPrincipal">	
			<?php
				include_once("php/contact.htm");
			?>
		</div>		
		
		<!-- Bas de page -->
		<?php
			include_once("php/reseaux.htm");
			include_once("php/piedPage.htm");
		?>			
	</div>
	
	<?php
		include_once("php/jQuery.htm");
	?>
	<script  type="text/javascript">
		// <![CDATA[
			menuContact();
		// ]]>
	</script>
</body>
</html>