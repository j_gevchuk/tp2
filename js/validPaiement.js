$(document).ready(function(){
	var nonValid;

	if ($.browser.webkit) {
    	$("input").attr('autocomplete','off');
	}

	$("form[name='paiement']").each(function(){
		var form = $(this);
		form.find('.rfield').addClass('non-valid');
	})

	$("input[name='paiement_panier']").click(function(e){
		$("form[name='paiement'] input").each(function(){
			if($(this).val() == ''){
				msgError($(this));
			}
			else {
				validerChamps($(this));
			}
		});
		nonValid = $('.paiement').find('.non-valid').size();
		if(nonValid <= 1){
			document.paiement.submit();
		}
	})

	function msgError(elt){
		$(elt).addClass('non-valid');
		$(elt).prev().addClass('msgError');
		$(elt).next().addClass('msgError').text(' *');
	}

	function supprimError(elt){
		$(elt).removeClass('non-valid');
		$(elt).prev().removeClass('msgError');
		$(elt).next().text('');
	}

	function validerChamps(elt){
		isNotNumber();
		isZip();
		isCredit();
		isCVV();
	}

//Fonction valide si les champs de texte ne contient que des lettres
//Dans le champs RUE des chiffres sont accéptées
	function isNotNumber(){
		//Valider 'Rue' (les chiffres sont acceptées)
		$("input[name='STREET']").bind('focusout', function(){
			var reg = /^[A-ÿ0-9 ]+$/;
			var sStreet = $(this).val();
			if(!reg.test(sStreet)){
				msgError($("input[name='STREET']"));
			}
			else {
				supprimError($("input[name='STREET']"));
			}
		})
		//Valider 'Ville', 'Province', 'Nom' et 'Prenom' (les chiffres sont acceptées)
		$("input[name='CITY'], input[name='STATE'], input[name='LASTNAME'], input[name='FIRSTNAME']").bind('focusout', function(){
			var elt = $(this);
			var reg = /^[a-zA-ZA-ÿ ]+$/;
			var sElt = elt.val();
			console.log(sElt);
			if(!reg.test(sElt)){
				msgError(elt);
			}
			else {
				supprimError(elt);
			}
		});
	}

	function isZip(){
		$("input[name='ZIP']").bind('focusout', function(){
			var zip = $("input[name='ZIP']");
			var str = $(zip).val();
			str = str.replace(/\s+/g, '').toUpperCase();
			$(zip).val(str);
			var reg;
			if($("select[name='COUNTRYCODE']").val() == 'CA'){
				reg = /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]\d[ABCEGHJKLMNPRSTVWXYZ]\d$/;
			}
			else if ($("select[name='COUNTRYCODE']").val() == 'US'){
				reg = /^\d{5}(-\d{4})?$/;
			}
			if(reg.test(str)){
				supprimError(zip);
			}
			else {
				msgError(zip);
			}
		})
	}

	function isCredit(){
		$("input[name='ACCT']").bind('focusout', function(){
			var credit = $("input[name='ACCT']");
			var sCredit = credit.val().replace(/\s+/g, '');
			if(isNaN(sCredit) || sCredit.length < 16 || sCredit == ''){
				msgError(credit);
			}
			else {
				supprimError(credit);
			}
		})
	}

	function isCVV(){
		$("input[name='CVV2']").bind('focusout', function(){
			var cvv = $("input[name='CVV2']");
			var sCVV = cvv.val().replace(/\s+/g, '');
			if(isNaN(sCVV) || sCVV.length < 3 || sCVV == ''){
				msgError(cvv);
			}
			else {
				supprimError(cvv);
			}
		})
	}
})


